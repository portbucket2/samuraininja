﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemySpwaner : MonoBehaviour
{
    public float delay = 6f;
    public int maxEnemy = 5;
    public List<GameObject> enemies;

    bool spwaned = false;
    float temp = 0f;
    bool keepSpwanning = true;

    GameController gameController;
    PlayerController playerController;
    LevelSpwanData spwanData;

    [SerializeField] int currentLevel;
    int spwanedCount = 0;
    void Start()
    {
        gameController = GameController.GetController();
        playerController = gameController.GetPlayer();
        spwanData = gameController.GetSpwanData();

        if (GameManager.GetManager())
            currentLevel = GameManager.GetManager().GetlevelCount() - 1;
        else
            currentLevel = 0;
        
        delay = spwanData.spwanDataPerLevel[currentLevel].spwanDelay;
        maxEnemy = spwanData.spwanDataPerLevel[currentLevel].totalEnemies;
        SpwanNewEnemy();
    }

    // Update is called once per frame
    void Update()
    {
        if (keepSpwanning)
        {
            if (!spwaned && temp > delay)
            {
                spwaned = true;
                SpwanNewEnemy();
            }
            else
            {
                temp += Time.deltaTime;
            }
            if (temp > delay * 2f)
            {
                temp = 0f;
                spwaned = false;
            }
        }
    }

    Vector3 RandomNavmeshLocation(float radius)
    {
        Vector3 randomDirection = playerController.transform.position + Random.insideUnitSphere * radius;
        randomDirection += transform.position;
        NavMeshHit hit;
        Vector3 finalPosition = Vector3.zero;
        if (NavMesh.SamplePosition(randomDirection, out hit, radius, 1))
        {
            finalPosition = hit.position;
        }
        return finalPosition;
    }
    void SpwanNewEnemy()
    {

        if (spwanedCount < spwanData.spwanDataPerLevel[currentLevel].perSpwan.Count)
        {
            int spwanNumber = spwanData.spwanDataPerLevel[currentLevel].perSpwan[spwanedCount];
            for (int i = 0; i < spwanNumber; i++)
            {
                GameObject ene = GetRandomEnemy();
                GameObject gg = Instantiate(ene, RandomNavmeshLocation(Random.Range(30f, 40f)), ene.transform.rotation, this.transform);
            }
            spwanedCount++;
        }
    }
    GameObject GetRandomEnemy()
    {
        return enemies[Random.Range(0, enemies.Count)];
    }

    public int GetMaxNumberOfEnemy()
    {
        return maxEnemy;
    }
    public void StopSpwanning()
    {
        keepSpwanning = false;
    }

}
