﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialController : MonoBehaviour
{
    [SerializeField] GameObject tutorialPanel;
    [SerializeField] Button tapButton;

    GameManager gameManager;

    void Start()
    {
        gameManager = GameManager.GetManager();
        if (gameManager.GetDataManager().GetGamePlayer.handTutorialShown)
        {
            tutorialPanel.SetActive(false);
            Time.timeScale = 1f;
        }
        else
        {
            tutorialPanel.SetActive(true);
            Time.timeScale = 0f;
        }

        tapButton.onClick.AddListener(delegate {
            tutorialPanel.SetActive(false);
            Time.timeScale = 1f;
        });

    }
    
}
