﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour
{
    [SerializeField] GameObject LevelCompletePanel;
    [SerializeField] GameObject deathPanel;
    [SerializeField] Button nextButton;
    [SerializeField] Button restartButton;

    GameController gameController;
    GameManager gameManager;
    AnalyticsController analyticsController;

    void Start()
    {
        gameManager = GameManager.GetManager();
        gameController = GameController.GetController();
        Buttons();
        analyticsController = AnalyticsController.GetController();        
    }

    public void LevelComplete()
    {
        StartCoroutine(LevelCompleteRoutine());
    }
    IEnumerator LevelCompleteRoutine()
    {
        yield return new WaitForSeconds(1f);
        LevelCompletePanel.SetActive(true);
        gameController.GetPlayer().LookAtCamera();
    }
    public void PlayerDead()
    {
        StartCoroutine(DeathRoutine());
    }
    IEnumerator DeathRoutine()
    {
        yield return new WaitForSeconds(1f);
        deathPanel.SetActive(true);
        gameController.GetPlayer().LookAtCamera();
    }

    void Buttons()
    {
        nextButton.onClick.AddListener(delegate {
            if (gameManager)
            {
                gameManager.GotoNextStage();
                analyticsController.LevelEnded();
            }
            else
            {
                SceneManager.LoadScene(1);
            }
            
	    });
        restartButton.onClick.AddListener(delegate {
            if (gameManager)
            {
                gameManager.ResetStage();
                analyticsController.LevelFailed();
            }
            else
            {
                SceneManager.LoadScene(1);
            }
        });
    }
}
