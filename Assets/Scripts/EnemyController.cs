﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;

public class EnemyController : MonoBehaviour
{
    [SerializeField] bool isRunner = false;
    [SerializeField] bool isAlive = true;
    [SerializeField] ENEMYTYPES enemyType;
    [SerializeField] Material insideMat;
    [SerializeField] float followDistance = 20f;
    [SerializeField] float moveSpeed = 10f;
    [SerializeField] float shootingDelay = 3f;
    [SerializeField] GameObject bullet;
    [SerializeField] ParticleSystem bloodParticle;
    [SerializeField] GameObject enemyBody;

    [Header("For debug Only: Autofill")]
    public Transform withHands;
    public Transform withoutHands;
    public Animator animator;
    public NavMeshAgent agent;
    public float invokeTime = 0f;
    public bool isFollowingStared = false; 

    PlayerController player;
    GameController gameController;
    float distance = 0f;
    float shootingCount = 5f;
    bool isFollowing = false;

    readonly string IDLE = "idle";
    readonly string WALK = "walk";
    readonly string SHOOT = "shoot";
    readonly string SLAP = "slap";

    void Start()
    {
        gameController = GameController.GetController();
        player = gameController.GetPlayer();
        withHands = transform.GetChild(0).transform;
        animator = transform.GetChild(0).GetComponent<Animator>();
        withoutHands = transform.GetChild(1).transform;
        agent = transform.GetComponent<NavMeshAgent>();
        transform.DOLookAt(player.transform.position, 0.1f);

        animator.SetTrigger(IDLE);
        animator.speed = 1f;

        Invoke("ShowEnemy", 0.3f);
    }
    void ShowEnemy()
    {
        enemyBody.SetActive(true);
        if (isRunner)
        {
            WalkingStart();
        }
        else
        {
            animator.SetTrigger(IDLE);
            animator.speed = 1f;
        }
    }
    void WalkingStart()
    {
        animator.SetTrigger(WALK);
        animator.speed = 0.8f;
        agent.speed = 5f;
        isFollowingStared = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (isAlive)
        {
            if (isRunner)
            {
                distance = Vector3.Distance(transform.position, player.transform.position);
                if (distance < followDistance)
                {
                    isFollowing = true;
                }
                else
                {
                    isFollowing = false;
                }

                if (isFollowing && isFollowingStared)
                {
                    //transform.DOLocalMove(player.transform.position, distance / moveSpeed).SetEase(Ease.Flash).OnComplete(() => DamagePlayer());
                    transform.DOLookAt(player.transform.position, 0.1f);
                    agent.SetDestination(player.transform.position);
                    // check distance
                    CheckDistance();
                }
            }
            else
            {
                if (shootingCount > shootingDelay)
                {
                    ShootBullet();
                }
                else
                {
                    shootingCount += Time.deltaTime;
                }
            }
        }
    }
    bool damageDone = false;
    void DamagePlayer()
    {
        if (!damageDone)
        {
            Debug.Log("damagingPlayer");
            damageDone = true;
            animator.SetTrigger(SLAP);
            gameController.DamagePlayer();
        }
        
    }
    void ShootBullet()
    {
        shootingCount = 0f;
        Vector3 direction = player.transform.position - transform.position;
        GameObject gg = Instantiate(bullet, transform.position, bullet.transform.rotation, null);
        gg.GetComponent<Bullet>().Shoot(this.transform,direction, 1f);
        transform.DOLookAt(player.transform.position, 0.1f);
        animator.SetTrigger(SHOOT);
    }
    void CheckDistance()
    {
        if (Vector3.Distance(transform.position, player.transform.position)< 2.5f )
        {
            agent.SetDestination(transform.position);
            DamagePlayer();
        }
        else
        {
            damageDone = false;
            if (!animator.GetCurrentAnimatorStateInfo(0).IsName("EnemyWalk") )
            {
                animator.SetTrigger(WALK);
            }
        }
    }

    public void EnemyDefeated(ExampleUseof_MeshCut sword, bool _isHorizontalSwipe)
    {
        bloodParticle.Play();
        withHands.gameObject.SetActive(false);
        withoutHands.gameObject.SetActive(true);

        sword.MeshCutObject(withoutHands.gameObject, insideMat, _isHorizontalSwipe);

        isAlive = false;
        transform.DOKill();
        if (agent.isActiveAndEnabled)
        {
            agent.SetDestination(transform.position);
        }
        agent.enabled = false;
        gameController.CheckLevelCompletion();
    }
    public ENEMYTYPES GetEnemyType()
    {
        return enemyType;
    }
    public Material GetInsideMat()
    {
        return insideMat;
    }
    public bool IsAlive()
    {
        return isAlive;
    }
}
public enum ENEMYTYPES
{
    Apple,
    Capsicum,
    Eggplant,
    Strawberry,
    Tomato
}
