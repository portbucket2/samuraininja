﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager gameManager;

    //[SerializeField] WeildingStyle weildingStyle;
    [SerializeField] bool tutorialShown = false;
    


    [Header("For Debugging")]
    public int levelcount = 1;
    public int totalScenes = 0;
    public int levelProgressionCount = 0;
    [SerializeField] DataManager dataManager;
    [SerializeField] GamePlayer gamePlayer;


    private void Awake()
    {
        gameManager = this;
        DontDestroyOnLoad(this.gameObject);
        dataManager = GetComponent<DataManager>();
        gamePlayer = new GamePlayer();
    }
    void Start()
    {
        totalScenes = SceneManager.sceneCountInBuildSettings;
        
        gamePlayer = dataManager.GetGamePlayer;
        if (gamePlayer.handTutorialShown)
        {
            TutorialSeen();
        }
        //Load last played level
        levelcount = gamePlayer.lastPlayedLevel;
        SceneManager.LoadScene(levelcount);
    }

    public static GameManager GetManager()
    {
        return gameManager;
    }
    public void GotoNextStage()
    {
        if (levelcount < totalScenes - 1) 
            levelcount++;
        else
            levelcount = 1;

        SceneManager.LoadScene(levelcount);

        levelProgressionCount++;
    }
    public int GetLevelProgressionCount()
    {
        return levelProgressionCount;
    }
    public void ResetStage()
    {
        SceneManager.LoadScene(levelcount);
    }
    public int GetlevelCount()
    {
        return levelcount;
    }
    public bool TutorialAlreadySeen()
    {
        return tutorialShown;
    }
    public void TutorialSeen()
    {
        tutorialShown = true;
    }
    public DataManager GetDataManager()
    {
        return dataManager;
    }
}
