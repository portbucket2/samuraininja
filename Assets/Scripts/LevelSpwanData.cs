﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Spwan Data", menuName = "Spwan Data", order = 51)]
public class LevelSpwanData : ScriptableObject
{
    public List <SpwanData> spwanDataPerLevel;
}

[System.Serializable]
public class SpwanData
{
    public int levelNumber;
    public int totalEnemies;
    public float spwanDelay;
    public List<int> perSpwan;
    public List<int> numberOfRange;
}
