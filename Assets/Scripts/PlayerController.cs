﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class PlayerController : MonoBehaviour
{
    
    [SerializeField] ExampleUseof_MeshCut sword;
    [SerializeField] Transform cameraCenteringPos;
    [SerializeField] ParticleSystem slashParticle;
    [SerializeField] ParticleSystem teleportParticleStart;
    [SerializeField] ParticleSystem teleportParticleEnd;
    [SerializeField] ParticleSystem slappedParticle;
    [SerializeField] SkinnedMeshRenderer playerMesh;
    [SerializeField] MeshRenderer swordMesh;
    [SerializeField] Animator animator;

    [Header("Gameplay Values:")]
    [SerializeField] float damageRadius = 2f;

    [Header("For Debug Only:")]
    [SerializeField] bool inputEnabled = true;
    [SerializeField] ParticleSystem swipeTrailParticle;
    [SerializeField] Camera mainCamera;
    [SerializeField] CapsuleCollider collider;
    [SerializeField] List<Transform> detectedEnemies;

    [Header("PlayerHealth: ")]
    [SerializeField] float currentHealth = 10f;
    [SerializeField] float maxHealth = 10f;

    Vector3 moveOffset = new Vector3(0f,0f,3f);
    readonly string CUTTABLE = "cuttable";
    readonly string ENEMY = "Enemy";
    readonly string BULLET = "Bullet";
    readonly string PLATFORM = "platform";

    readonly string IDLE = "idle";
    readonly string ATTACK = "attack";

    bool isDetecting = false;
    bool isSwipeHorizontal = true;
    Vector3 mouseStart;
    Vector3 mouseEnd;

    WaitForEndOfFrame ENDOFFRAME = new WaitForEndOfFrame();
    WaitForSeconds WAITPOINTONE = new WaitForSeconds(0.1f);
    WaitForSeconds WAITHALF = new WaitForSeconds(0.5f);

    GameController gameController;
    AnalyticsController analyticsController;

    void Start()
    {
        gameController = GameController.GetController();
        swipeTrailParticle = gameController.GetSwipeTrailParticle();
        mainCamera = gameController.GetMainCamera();

        analyticsController = AnalyticsController.GetController();
        analyticsController.LevelStarted();

        collider = GetComponent<CapsuleCollider>();
    }


    void Update()
    {
        if (inputEnabled)
        {
            if (Input.GetMouseButtonDown(0))
            {
                isDetecting = true;
                detectedEnemies = new List<Transform>();
                swipeTrailParticle.gameObject.SetActive(true);
                mouseStart = Input.mousePosition;
            }

            if (Input.GetMouseButton(0))
            {
                //if (isDetecting)
                //{
                //    Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
                //    RaycastHit hit;
                //    if (Physics.Raycast(ray, out hit))
                //    {
                //        Debug.DrawLine(ray.origin, hit.point, Color.green, 2f);
                //        if (hit.transform.CompareTag(CUTTABLE))
                //        {
                //            // add to list
                //            if (!detectedEnemies.Contains(hit.transform))
                //            {
                //                detectedEnemies.Add(hit.transform);
                //            }
                //        }
                //        swipeTrailParticle.transform.position = hit.point;
                //    }
                //}
            }

            if (Input.GetMouseButtonUp(0))
            {
                mouseEnd = Input.mousePosition;
                isDetecting = false;
                swipeTrailParticle.gameObject.SetActive(false);
                //int max = detectedEnemies.Count;
                //if (max > 0)
                //{
                //    Vector3 posTotal = Vector3.zero;
                //    Vector3 posAvg = Vector3.zero;
                //    for (int i = 0; i < max; i++)
                //    {
                //        posTotal += detectedEnemies[i].position;
                //    }
                //    posAvg = posTotal / max;
                //    SlashingMove(posAvg - moveOffset);
                //}
                Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.transform.CompareTag(BULLET))
                    {
                        SlashingMove(hit.transform.GetComponent<Bullet>().GetShooter().position);
                    }
                    if (hit.transform.CompareTag(PLATFORM) ) 
                    {
                        SlashingMove(hit.point);
                    }
                    if (hit.transform.CompareTag(CUTTABLE))
                    {
                        // add to list
                        SlashingMove(hit.transform.position - moveOffset);
                    }
                }
            }
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag(ENEMY))
        {
            Debug.LogError("hit Name: " + other.name);
            EnemyController ec = other.GetComponent<EnemyController>();
            ec.EnemyDefeated(sword, isSwipeHorizontal);
            slashParticle.Play();
        }
    }

    void SlashingMove(Vector3 hitTransform)
    {
        playerMesh.enabled = false;
        swordMesh.enabled = false;
        teleportParticleStart.Play();
        collider.isTrigger = true;
        transform.DOLocalMove(NextPosition(hitTransform), SlashMoveTime(hitTransform)).SetEase(Ease.Flash).OnComplete(() => MoveComplete());        
    }
    float SlashMoveTime(Vector3 enemy)
    {
        float time = 0.5f;
        float distance = Vector3.Distance(enemy, transform.position);
        if (distance > 20f)
        {
            time = 0.25f;
            //Debug.LogError("far");
        }
        else
        {
            time = 0.1f;
            //Debug.LogError("near");
        }
        return time;
    }
    void MoveComplete()
    {
        playerMesh.enabled = true;
        swordMesh.enabled = true;
        teleportParticleEnd.Play();
        UpdateCameraCentering();
        
        animator.SetTrigger(ATTACK);
        DetectSwipeDirection();
        collider.isTrigger = false;
        //MeshCut();
        gameController.SetFocusPoint(transform.position);
    }

    void MeshCut( )
    {
        StartCoroutine(MeshCutRoutine());
    }
    IEnumerator MeshCutRoutine()
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, damageRadius);
        foreach (var hitCollider in hitColliders)
        {
            if (hitCollider.transform.CompareTag(CUTTABLE))
            {
                transform.LookAt(hitCollider.transform);
                EnemyController ec = hitCollider.GetComponentInParent<EnemyController>();
                ec.EnemyDefeated(sword, isSwipeHorizontal);
                
                //Debug.LogError("hitName: " + hitCollider.name);
            }

        }
        yield return WAITPOINTONE;

        //hitColliders = Physics.OverlapSphere(transform.position, damageRadius);
        //foreach (var hitCollider in hitColliders)
        //{
        //    if (hitCollider.transform.CompareTag(CUTTABLE))
        //    {
        //        transform.LookAt(hitCollider.transform);
        //        EnemyController ec = hitCollider.GetComponentInParent<EnemyController>();
        //        //sword.MeshCutRaycast(ec.GetInsideMat());
        //        sword.MeshCutObject(hitCollider.gameObject, ec.GetInsideMat(), isSwipeHorizontal);
        //        //hitCollider.GetComponentInParent<EnemyController>().EnemyDefeated();
        //        Debug.DrawLine(transform.position, hitCollider.transform.position, Color.yellow, 2f);
        //        //Debug.LogError("hitName: " + hitCollider.name);
        //    }

        //}
        yield return ENDOFFRAME;

        //level completion check
        //gameController.CheckLevelCompletion();
    }
    void DetectSwipeDirection()
    {
        float hr = Mathf.Abs(mouseStart.x - mouseEnd.x);
        float vr = Mathf.Abs(mouseStart.y - mouseEnd.y);
        if (hr > vr)
        {
            isSwipeHorizontal = false;
        }
        else
        {
            isSwipeHorizontal = true;
        }
    }
    Vector3 NextPosition(Vector3 targetpos)
    {
        Vector3 temp = new Vector3(targetpos.x, 0f, targetpos.z)  - moveOffset;
        return temp;
    }
    void UpdateCameraCentering()
    {
        cameraCenteringPos.DOMove(new Vector3(0f, 0f, transform.position.z + 10f), 0.2f).SetEase(Ease.Flash) ;
    }

    public void LookAtCamera()
    {
        Vector3 turn = new Vector3(0f, 180f, 0f);
        transform.DOLocalRotate(turn, 0.5f).SetEase(Ease.Flash);
        inputEnabled = false;
    }
    public void DamagePlayer()
    {
        currentHealth--;
        HealthCheck();
        slappedParticle.Play();
    }
    public void ResetHealth()
    {
        currentHealth = maxHealth;
    }
    void HealthCheck()
    {
        if (currentHealth <= 0)
        {
            Debug.Log("Player dead!!!");
            gameController.PlayerDead();
        }
    }
}
