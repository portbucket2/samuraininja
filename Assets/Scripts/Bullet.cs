﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] float speed = 10f;
    Transform shooter;
    Rigidbody rb;
    readonly string PLAYER = "Player";
    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
    }
    void Start()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.CompareTag(PLAYER))
        {
            Debug.Log("bullet hit player!!");
            gameObject.SetActive(false);
        }
    }
    public Transform GetShooter()
    {
        return shooter;
    }

    public void Shoot(Transform _parent, Vector3 direction, float _speed)
    {
        transform.LookAt(direction);
        rb.isKinematic = false;
        shooter = _parent;
        rb.velocity = direction * speed;
    }
}
