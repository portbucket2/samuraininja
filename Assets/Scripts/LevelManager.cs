﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    

    [Header("For Debug Only: ")]
    [SerializeField] EnemySpwaner enemySpwaner;
    [SerializeField] int max = 0;
    [SerializeField] int count = 0;
    [SerializeField] List<EnemyController> enemies;

    GameController gameController;

    

    // Start is called before the first frame update
    void Start()
    {
        gameController = GameController.GetController();
        enemySpwaner = gameController.GetEnemySpwaner();
        enemies = new List<EnemyController>();
        max = enemySpwaner.GetMaxNumberOfEnemy();
        

    }

    public void CheckLevelCompletion()
    {
        count++;
        if (count >= max) 
        {
            Debug.LogError("All dead!!!");
            gameController.GetEnemySpwaner().StopSpwanning();
            gameController.GetUIController().LevelComplete();            
        }
    }

}
