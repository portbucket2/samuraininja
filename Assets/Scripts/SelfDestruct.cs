﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SelfDestruct : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        transform.DOScale(0.01f, 10f).SetEase(Ease.InSine).OnComplete(OnShrinked);
    }
    void OnShrinked()
    {
        Destroy(this.gameObject);
    }

}
