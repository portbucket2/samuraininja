﻿using UnityEngine;
using System.Collections;

public class ExampleUseof_MeshCut : MonoBehaviour {

	public PhysicMaterial bounce;
	public Material insideMat;
	public float explosion = 2f;
	public float gizmoDistance = 10f;
	public float slicedObjectMass = 1f;

	readonly string UNTAGGED = "Untagged";
	readonly string CUTTABLE = "cuttable";
	// Use this for initialization
	void Start () {

		
	}
	
	void Update(){

		if(Input.GetMouseButtonDown(0)){

			//MeshCut();
		}
	}

    public void MeshCutRaycast(Material insideMat)
    {
		RaycastHit hit;

		if (Physics.Raycast(transform.position, transform.forward, out hit))
		{
			this.insideMat = insideMat;
			GameObject victim = hit.collider.gameObject;

            GameObject[] pieces = BLINDED_AM_ME.MeshCut.Cut(victim, transform.position, transform.right, this.insideMat);

			if (!pieces[0].GetComponent<Rigidbody>())
			{
				//Destroy(pieces[0].GetComponent<MeshCollider>());
				pieces[0].GetComponent<MeshCollider>().sharedMesh = null;
				pieces[0].GetComponent<MeshCollider>().sharedMesh = pieces[0].GetComponent<MeshFilter>().mesh;
				pieces[0].GetComponent<MeshCollider>().convex = true;

				pieces[0].AddComponent<Rigidbody>();
				pieces[0].GetComponent<Rigidbody>().mass = slicedObjectMass;
				pieces[0].GetComponent<Rigidbody>().AddForce(transform.right * explosion, ForceMode.Impulse);
				pieces[0].transform.gameObject.tag = UNTAGGED;
			}

			if (!pieces[1].GetComponent<Rigidbody>())
			{
				pieces[1].AddComponent<MeshCollider>();
				pieces[1].GetComponent<MeshCollider>().convex = true;

				pieces[1].AddComponent<Rigidbody>();
				pieces[1].GetComponent<Rigidbody>().mass = slicedObjectMass;
				pieces[1].GetComponent<Rigidbody>().AddForce((-1f) * transform.right * explosion, ForceMode.Impulse);
                pieces[1].transform.gameObject.tag = UNTAGGED;
            }

			//Destroy(pieces[1], 5);
		}
	}
	public void MeshCutObject(GameObject _gameObject, Material insideMat, bool isSwipeHorizontal)
	{
        this.insideMat = insideMat;
        GameObject victim = _gameObject;
		Vector3 slashDirection = Vector3.right;
        if (isSwipeHorizontal)
			slashDirection = Vector3.right;
        else
			slashDirection = Vector3.up;

        GameObject[] pieces = BLINDED_AM_ME.MeshCut.Cut(victim, victim.transform.position, slashDirection, this.insideMat);

		if (!pieces[0].GetComponent<Rigidbody>())
		{
			//Destroy(pieces[0].GetComponent<MeshCollider>());
			pieces[0].GetComponent<MeshCollider>().sharedMesh = null;
			pieces[0].GetComponent<MeshCollider>().sharedMesh = pieces[0].GetComponent<MeshFilter>().mesh;
			pieces[0].GetComponent<MeshCollider>().convex = true;
			pieces[0].GetComponent<MeshCollider>().material = bounce;

			pieces[0].AddComponent<Rigidbody>();
			pieces[0].GetComponent<Rigidbody>().mass = slicedObjectMass;
			pieces[0].GetComponent<Rigidbody>().AddForce(transform.right * explosion, ForceMode.Impulse);
			pieces[0].transform.gameObject.tag = UNTAGGED;
			pieces[0].gameObject.AddComponent<SelfDestruct>();
			pieces[0].gameObject.layer = 2;
			//pieces[0].transform.GetComponent<MeshRenderer>().material = outsideMat;
		}

		if (!pieces[1].GetComponent<Rigidbody>())
		{
			pieces[1].AddComponent<MeshCollider>();
			pieces[1].GetComponent<MeshCollider>().convex = true;
			pieces[1].GetComponent<MeshCollider>().material = bounce;

			pieces[1].AddComponent<Rigidbody>();
			pieces[1].GetComponent<Rigidbody>().mass = slicedObjectMass;
			pieces[1].GetComponent<Rigidbody>().AddForce((-1f) * transform.right * explosion, ForceMode.Impulse);
			pieces[1].transform.gameObject.tag = UNTAGGED;
			pieces[1].gameObject.AddComponent<SelfDestruct>();
			pieces[1].gameObject.layer = 2;
			//pieces[1].transform.GetComponent<MeshRenderer>().material = outsideMat;
		}
	}



	void OnDrawGizmosSelected() {

		Gizmos.color = Color.red;

		Gizmos.DrawLine(transform.position, transform.position + transform.forward * gizmoDistance);
		Gizmos.DrawLine(transform.position + transform.up * 0.5f, transform.position + transform.up * 0.5f + transform.forward * 5.0f);
		Gizmos.DrawLine(transform.position + -transform.up * 0.5f, transform.position + -transform.up * 0.5f + transform.forward * 5.0f);

		Gizmos.DrawLine(transform.position, transform.position + transform.up * 0.5f);
		Gizmos.DrawLine(transform.position,  transform.position + -transform.up * 0.5f);

	}

}
