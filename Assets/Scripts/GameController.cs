﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public static GameController gameController;
    [SerializeField] PlayerController playerController;
    [SerializeField] LevelManager levelManager;
    [SerializeField] UIController uiController;
    [SerializeField] ParticleSystem swipeTrailParticle;
    [SerializeField] Camera mainCamera;
    [SerializeField] EnemySpwaner enemySpwaner;
    [SerializeField] LevelSpwanData spwanData;
    [SerializeField] Transform focusPoint;


    private void Awake()
    {
        gameController = this;
    }
    void Start()
    {
        Debug.Log("Total enenmies: " + spwanData.spwanDataPerLevel[0].totalEnemies);
    }
    public static GameController GetController()
    {
        return gameController;
    }
    public PlayerController GetPlayer()
    {
        return playerController;
    }
    public UIController GetUIController()
    {
        return uiController;
    }
    public ParticleSystem GetSwipeTrailParticle()
    {
        return swipeTrailParticle;
    }
    public Camera GetMainCamera()
    {
        return mainCamera;
    }
    public EnemySpwaner GetEnemySpwaner()
    {
        return enemySpwaner;
    }
    public LevelSpwanData GetSpwanData()
    {
        return spwanData;
    }
    public void CheckLevelCompletion()
    {
        levelManager.CheckLevelCompletion();
    }
    public void DamagePlayer()
    {
        playerController.DamagePlayer();
    }
    public void PlayerDead()
    {
        Debug.LogError("Player dead!!!");
        GetEnemySpwaner().StopSpwanning();
        GetUIController().PlayerDead();
    }
    public void SetFocusPoint(Vector3 playerPosition)
    {
        Vector3 pp = new Vector3(0f,0f,playerPosition.z + 10f);
        focusPoint.position = pp;
    }
}
