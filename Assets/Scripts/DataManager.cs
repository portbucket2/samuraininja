﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class DataManager : MonoBehaviour
{
    public bool showPersistentDataPath = false;
    const string gamePlayerDat = "/gamePlayer.dat";
    [Header("Debug Data:")]
    [SerializeField] GamePlayer gamePlayer;
    private void Awake()
    {
        gamePlayer = new GamePlayer();
        LoadData();
    }
    void Start()
    {
        if (showPersistentDataPath)
        {
            Debug.Log("Data Path: " + Application.persistentDataPath);
        }
    }
    public GamePlayer GetGamePlayer
    {
        get
        {
            return gamePlayer;
        }
    }

    bool IsDataAvailable()
    {
        if (File.Exists(Application.persistentDataPath + gamePlayerDat))
        {
            return true;
        }
        else
            return false;
    }

    public void SaveData()
    {
        BinaryFormatter bf = new BinaryFormatter();

        FileStream file = File.Create(Application.persistentDataPath + gamePlayerDat);
        bf.Serialize(file, gamePlayer);
        file.Close();

    }
    public void LoadData()
    {
        LoadGamePlayer();
    }
    void LoadGamePlayer()
    {
        //save location on disk
        Debug.Log("persistenetDatapath:" + Application.persistentDataPath);
        BinaryFormatter bf = new BinaryFormatter();

        if (File.Exists(Application.persistentDataPath + gamePlayerDat))
        {
            FileStream file = File.Open(Application.persistentDataPath + gamePlayerDat, FileMode.Open);
            gamePlayer = (GamePlayer)bf.Deserialize(file);
            file.Close();
        }
        else
        {
            gamePlayer = new GamePlayer();
            gamePlayer.name = "";
            gamePlayer.id = 1;
            gamePlayer.levelsCompleted = 1;
            gamePlayer.totalStars = 0;
            gamePlayer.lastPlayedLevel = 1;
        }
    }
    public void SetGameplayerData(GamePlayer gg)
    {
        gamePlayer = new GamePlayer();
        gamePlayer = gg;

        SaveData();
    }
}
